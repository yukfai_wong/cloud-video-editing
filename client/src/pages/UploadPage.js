import React, { useState } from 'react';
import {
  Button,
  Stepper,
  StepLabel,
  Step,
  Typography,
} from '@material-ui/core';
import axios from 'axios';
import '../App.css';
import ImageSettings from '../components/UploadPage/ImageSettings';
import UploadVideo from '../components/UploadPage/UploadVideo';
import VideoUploading from '../components/UploadPage/VideoUploading';
import NotLoggedIn from '../components/NotLoggedIn';

export default function UploadPage() {
  const [videoUploaded, setVideoUploaded] = useState(false);
  const [uploadData, setUploadData] = useState({});

  const [activeStep, setActiveStep] = useState(0);
  const steps = getSteps();

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  function getSteps() {
    return ['Upload a video', 'Uploading video'];
  }

  function getStepContent(stepIndex) {
    switch (stepIndex) {
      case 0:
        return (
          <UploadVideo
            uploadData={setUploadData}
            nextPage={handleNext}
            videoUploaded={setVideoUploaded}
          />
        );
      case 1:
        return <VideoUploading uploadComplete={videoUploaded} />;
      default:
        return 'Unknown stepIndex';
    }
  }

  // Means the the user isnt logged in
  if (!localStorage.getItem('token')) {
    return <NotLoggedIn />;
  }
  return (
    <div className="UploadPage">
      <Stepper activeStep={activeStep} alternativeLabel>
        {steps.map((label) => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
      <div>
        {activeStep === steps.length ? (
          <div>
            <Typography>All steps completed</Typography>
            <Button onClick={handleReset}>Reset</Button>
          </div>
        ) : (
          <div className="uploadVideoBody">{getStepContent(activeStep)}</div>
        )}
      </div>
    </div>
  );
}
