import { TextField, Button, Snackbar } from '@material-ui/core';
import Axios from 'axios';
import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Alert } from '@material-ui/lab';
import AlertMessage from '../components/AlertMessage';

export default function Login() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [message, setMessage] = useState({});
  const [openMessage, setOpenMessage] = useState(false);

  const history = useHistory();

  const closeSnackbar = () => {
    setOpenMessage(false);
  };

  const login = () => {
    if (!email || !password) {
      setMessage({
        type: 'warning',
        message: 'Please make sure both the email and password are entered.',
      });
      setOpenMessage(true);
      return;
    }

    Axios.get('/users/login', {
      params: {
        email: email,
        password: password,
      },
    })
      .then(({ data }) => {
        if (data.accessToken) {
          //storing the JWT token here (not secure but will be fine for now)
          localStorage.setItem('token', data.accessToken);

          //redirect the user to the home page
          history.push('/');
          window.location.reload();
        }
      })
      .catch((err) => {
        // using an object so we can use the same error box for warnings and errors
        setMessage({
          type: 'error',
          message: 'The email or password does not exist on the system.',
        });
        setOpenMessage(true);
      });
  };

  return (
    <div className="Login">
      <form className="LoginForm" onSubmit={login}>
        <h3>Login</h3>
        <TextField
          className="input text"
          id="outlined-basic"
          label="Email"
          variant="outlined"
          onChange={(e) => setEmail(e.target.value)}
          type="email"
        />
        <TextField
          className="input text"
          id="outlined-basic"
          label="Password"
          variant="outlined"
          type="password"
          onChange={(e) => setPassword(e.target.value)}
        />
        <Button
          className="input button"
          variant="contained"
          color="primary"
          fullWidth
          onClick={login}
        >
          LOGIN
        </Button>
      </form>

      <AlertMessage
        message={message}
        onClose={closeSnackbar}
        openMessage={openMessage}
      />
    </div>
  );
}
