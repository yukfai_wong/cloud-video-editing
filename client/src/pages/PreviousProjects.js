import { CircularProgress } from '@material-ui/core';
import Axios from 'axios';
import React, { useState, useEffect } from 'react';
import ProjectItem from '../components/ProjectItem';
import '../App.css';
import NotLoggedIn from '../components/NotLoggedIn';

export default function PreviousProjects() {
  const [userVideo, setUserVideo] = useState([]);
  const [loading, setLoading] = useState(true);

  //getting the users uploaded videos
  useEffect(() => {
    Axios.get('/uploads/videos', {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('token'),
      },
    }).then((data) => {
      setUserVideo(data.data.Items);
      setLoading(false);
    });
  }, []);

  if (!localStorage.getItem('token')) {
    return <NotLoggedIn />;
  }

  if (loading) {
    return (
      <div className="loading">
        <CircularProgress />
      </div>
    );
  }

  //checking if the user has previous videos
  if (userVideo.length === 0) return <h1>No previous videos found.</h1>;

  return (
    <div className="previousProjects">
      {userVideo.map((video) => (
        <ProjectItem video={video} />
      ))}
    </div>
  );
}
