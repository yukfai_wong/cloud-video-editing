import { TextField, Button, Snackbar } from '@material-ui/core';
import Axios from 'axios';
import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Alert } from '@material-ui/lab';
import AlertMessage from '../components/AlertMessage';

export default function Login() {
  //Password state
  const [fName, setFName] = useState('');
  const [lName, setLName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirm, setConfirm] = useState('');

  const [message, setMessage] = useState({});
  const [openMessage, setOpenMessage] = useState(false);
  const history = useHistory();

  const closeSnackbar = () => {
    setOpenMessage(false);
  };

  //error state
  const [notMatch, setNotMatch] = useState(false);

  const addUser = () => {
    if (password !== confirm) {
      setNotMatch(true);
      setMessage({
        type: 'warning',
        message: 'Please make sure the passwords match.',
      });
      setOpenMessage(true);
      return;
    }
    if (!email || !fName || !lName || !password) {
      setMessage({
        type: 'warning',
        message: 'Please make sure all fields are entered.',
      });
      setOpenMessage(true);
      return;
    }

    Axios.get('/users/signup', {
      params: {
        email: email,
        password: password,
        firstName: fName,
        lastName: lName,
      },
    })
      .then((data) => {
        Axios.get('/users/login', {
          params: {
            email: email,
            password: password,
          },
        }).then(({ data }) => {
          if (data.accessToken) {
            //storing the JWT token here (not secure but will be fine for now)
            localStorage.setItem('token', data.accessToken);
            //redirect the user to the home page
            history.push('/');
          }
        });
      })
      .catch((err) => {
        setMessage({
          type: 'error',
          message: 'User with that email already exists.',
        });
        setOpenMessage(true);
      });
  };

  return (
    <div className="Login">
      <form className="LoginForm" onSubmit={addUser}>
        <h3>Sign Up</h3>
        <TextField
          className="input text"
          id="outlined-basic"
          label="First Name"
          variant="outlined"
          onChange={(e) => setFName(e.target.value)}
        />
        <TextField
          className="input text"
          id="outlined-basic"
          label="Last Name"
          variant="outlined"
          onChange={(e) => setLName(e.target.value)}
        />
        <TextField
          className="input text"
          id="outlined-basic"
          label="Email"
          variant="outlined"
          onChange={(e) => setEmail(e.target.value)}
          type="email"
        />
        <TextField
          className="input text"
          id="outlined-basic"
          label="Password"
          variant="outlined"
          onChange={(e) => setPassword(e.target.value)}
          error={notMatch}
          type="password"
        />
        <TextField
          className="input text"
          id="outlined-basic"
          label="Confirm Password"
          variant="outlined"
          onChange={(e) => setConfirm(e.target.value)}
          error={notMatch}
          type="password"
        />
        <Button
          className="input button"
          variant="contained"
          color="primary"
          fullWidth
          onClick={addUser}
        >
          SIGN UP
        </Button>
      </form>

      <AlertMessage
        message={message}
        onClose={closeSnackbar}
        openMessage={openMessage}
      />
    </div>
  );
}
