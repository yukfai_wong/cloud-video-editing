import React, { useState } from 'react';
import {
  Button,
  Snackbar,
  TextField,
  Switch,
  FormControlLabel,
} from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import AlertMessage from '../AlertMessage';

import Axios from 'axios';

export default function UploadVideo({ nextPage, videoUploaded }) {
  const [fileName, setFileName] = useState('Choose File...');
  const [file, setFile] = useState('');

  const [message, setMessage] = useState({});
  const [openMessage, setOpenMessage] = useState(false);
  const [imageText, setImageText] = useState('');
  const [xPos, setXPos] = useState(0);
  const [yPos, setYPos] = useState(0);
  const [blur, setBlur] = useState(false);
  const [blackWhite, setBlackWhite] = useState(false);
  const [scaleFactor, setScaleFactor] = useState(1);

  const closeSnackbar = () => {
    setOpenMessage(false);
  };

  const selectFile = (e) => {
    const file = e.target.files[0];
    console.log(file);
    if (file.size >= 50000000) {
      setOpenMessage(true);
      setMessage({
        type: 'error',
        message: 'File size is to large, please choose a smaller file',
      });
    } else {
      setFile(file);
      setFileName(file.name);
    }

    //making sure the file is not over capacity (50MB)
  };

  const uploadVideo = async () => {
    if ((imageText && !xPos) || (imageText && !yPos)) {
      setMessage({
        type: 'warning',
        message: 'Please make sure both the X and Y positions are entered',
      });
      setOpenMessage(true);
      return;
    }
    nextPage(true);

    //getting the user email from the JWT token and decoding it using the endpoint
    const userEmail = await Axios.get('/users/currentUser', {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('token'),
      },
    });

    //adding the video file to the form data
    const formData = new FormData();
    formData.append('file', file);

    Axios.post('/upload', formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      params: {
        text: imageText,
        x: xPos,
        y: yPos,
        resizeFactor: scaleFactor,
        blackWhite: blackWhite,
        blur: blur,
        user: userEmail.data,
      },
    })
      .then((res) => {
        // Updating the upload state to display to the user on the next screen
        videoUploaded(true);
      })
      .catch((err) => {
        console.log(err);
        setOpenMessage(true);
        setMessage({
          type: 'error',
          message:
            'No files were selected, please choose a video file to continue.',
        });
      });
  };

  return (
    <div className="UploadVideo">
      <h1>Convert Video Frames into Images</h1>
      <h1>Apply Effects to Images</h1>

      <p className="maxFile">Max File Size: 50MB</p>
      <div className="fileUpload">
        <div className="fileUploadTextDiv">
          <p>{fileName}</p>
        </div>
        <input
          id="contained-button-file"
          type="file"
          className="fileUploadHidden"
          onChange={selectFile}
        />
        <label className="buttonChooseFile" htmlFor="contained-button-file">
          <Button variant="contained" color="primary" component="span">
            Choose Video
          </Button>
        </label>
      </div>

      <div className="ImageSettings">
        <h2>Please choose image settings for custom filter</h2>

        <TextField
          className="input imageText"
          id="outlined-basic"
          label="Image Text"
          variant="outlined"
          onChange={(e) => setImageText(e.target.value)}
          fullWidth
        />
        <div className="textPositionContainer">
          <TextField
            className="input imageText"
            id="outlined-basic"
            label="X Position"
            variant="outlined"
            onChange={(e) => setXPos(e.target.value)}
            type="number"
          />
          <TextField
            className="input imageText"
            id="outlined-basic"
            label="Y Position"
            variant="outlined"
            onChange={(e) => setYPos(e.target.value)}
            type="number"
          />
          <TextField
            className="input imageText"
            id="outlined-basic"
            label="Scale Factor"
            variant="outlined"
            onChange={(e) => setScaleFactor(e.target.value)}
            type="number"
          />
          <FormControlLabel
            control={
              <Switch
                name="checkedC"
                onChange={(e) => setBlur(!blur)}
                color="primary"
              />
            }
            label="Blurred"
          />
          <FormControlLabel
            control={
              <Switch
                name="checkedC"
                onChange={(e) => setBlackWhite(!blackWhite)}
                color="primary"
              />
            }
            label="Black and White"
          />
        </div>
        <div className="uploadDiv">
          <Button
            variant="contained"
            color="primary"
            fullWidth
            onClick={uploadVideo}
          >
            Upload Video
          </Button>
        </div>
        <AlertMessage
          message={message}
          openMessage={openMessage}
          onClose={closeSnackbar}
        />
      </div>

      <AlertMessage
        message={message}
        openMessage={openMessage}
        onClose={closeSnackbar}
      />
    </div>
  );
}
