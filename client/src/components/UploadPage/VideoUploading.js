import React from 'react';
import { Button, CircularProgress } from '@material-ui/core';
import { useHistory } from 'react-router-dom';

export default function VideoUploading({ uploadComplete }) {
  const history = useHistory();

  return (
    <div className="videoUploaded">
      {uploadComplete ? (
        <div>
          <h2>Video Uploaded</h2>
          <p>
            Your video is know be processed.
            <br /> To check on the status of it please go to "Previous
            Projects".
          </p>
          <div className="uploadedButtons">
            <Button
              onClick={() => history.go(0)}
              variant="contained"
              color="primary"
            >
              Upload Another Video
            </Button>
            <Button
              onClick={() => history.push('/projects')}
              variant="contained"
              color="primary"
            >
              Check Progress of Video
            </Button>
          </div>
        </div>
      ) : (
        <div className="uploadingVideo">
          <CircularProgress />
          <p>Upload Video</p>
        </div>
      )}
    </div>
  );
}
