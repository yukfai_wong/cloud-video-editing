import {
  Button,
  CircularProgress,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Typography,
} from '@material-ui/core';
import React from 'react';
import { Link } from 'react-router-dom';
import AWS from 'aws-sdk';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Axios from 'axios';

export default function ProjectItem({ video }) {
  // Possibly add in an image to the S3 bucket to make it better if we have time
  const download = () => {
    Axios.get('/uploads/download', {
      params: {
        id: video.id,
      },
    }).then((data) => window.open(data.data));
  };

  return (
    <div className="projectItem">
      <div>
        <h2>Project Name:</h2>
        <h3>{video.originalFileName}</h3>
        <Accordion>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            className="filtersAccordian"
          >
            Filters
          </AccordionSummary>
          <AccordionDetails>
            <div className="filterContainer">
              {video.edit.text && <p>Image Text: {video.edit.text}</p>}
              {video.edit.x !== '0' && <p>X Positon: {video.edit.x}</p>}
              {video.edit.y !== '0' && <p>Y Positon: {video.edit.y}</p>}
              {video.resizeFactor !== '1' && (
                <p>Scale Factor: {video.resizeFactor}</p>
              )}
              {video.blur !== 'false' && <p>Blurred: {video.blur}</p>}
              {video.blackWhite !== 'false' && (
                <p>Black and White: {video.blackWhite}</p>
              )}
            </div>
          </AccordionDetails>
        </Accordion>

        <p>Uploaded on: {new Date(video.uploadedOn).toLocaleString()}</p>
      </div>
      {video.status === 'Finished' ? (
        <Button
          className="input download"
          variant="contained"
          color="primary"
          fullWidth
          onClick={download}
        >
          Download
        </Button>
      ) : (
        <div className="processingIndicator">
          <CircularProgress />
          <p>{video.status}</p>
        </div>
      )}
    </div>
  );
}
