import React, { useState } from 'react';
import { Button, Snackbar } from '@material-ui/core';
import { Alert } from '@material-ui/lab';

export default function AlertMessage({ message, openMessage, onClose }) {
  const [open, setOpen] = useState(openMessage);

  return (
    <div>
      <Snackbar open={openMessage} onClose={onClose}>
        <Alert onClose={onClose} severity={message.type}>
          {message.message}
        </Alert>
      </Snackbar>
    </div>
  );
}
