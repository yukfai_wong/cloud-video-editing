import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import '../App.css';

export default function NavBar() {
  const logout = () => {
    localStorage.removeItem('token');
    window.location.reload();
  };

  // logout();

  return (
    <div className="NavBar">
      <div className="leftLinks">
        <Link to="/" className="navLink">
          HOME
        </Link>
        <Link to="/upload" className="navLink">
          UPLOAD VIDEO
        </Link>
        <Link to="/projects" className="navLink">
          PREVIOUS PROJECTS
        </Link>
      </div>

      <div className="rightLinks">
        {localStorage.getItem('token') ? (
          <Link onClick={logout} className="navLink">
            LOGOUT
          </Link>
        ) : (
          <div className="rightLinks">
            <Link to="/login" className="navLink">
              LOGIN
            </Link>
            <Link to="/signup" className="navLink">
              SIGNUP
            </Link>
          </div>
        )}
      </div>
    </div>
  );
}
