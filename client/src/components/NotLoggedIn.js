import React from 'react';

export default function NotLoggedIn() {
  return (
    <div>
      <div>
        <h1>Not logged in. Please log in or sign up to use this feature.</h1>
      </div>
    </div>
  );
}
