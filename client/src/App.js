import React from 'react';
import UploadPage from './pages/UploadPage';
import LoginPage from './pages/Login';
import SignupPage from './pages/Signup';
import PreviousProjects from './pages/PreviousProjects';

import Nav from './components/NavBar';

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <Router>
        <Nav />
        <Switch>
          <div>
            <Route exact path="/" component={UploadPage} />
            <Route exact path="/upload" component={UploadPage} />
            <Route exact path="/login" component={LoginPage} />
            <Route exact path="/signup" component={SignupPage} />
            <Route exact path="/projects" component={PreviousProjects} />
          </div>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
