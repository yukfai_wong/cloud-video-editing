//Build in
const util = require('util');
const pipeline = util.promisify(require('stream').pipeline);
const exec = util.promisify(require('child_process').exec);

//Layers
const AWS = require('aws-sdk');
const fs = require('fs-extra');
const tar = require('tar');
const Jimp = require('jimp');


//Global
const s3 = new AWS.S3();
const dstS3 = "n10670815-edit-result";

const DB = new AWS.DynamoDB.DocumentClient();
const edit_table = "video-decoder-settings";

exports.handler = async function(event, context) {
  const new_record = event.Records[0];

  const split = new_record.s3.object.key.split('.');
  const partation_name = split.slice(0,1).join();
  const split_name = split.slice(0,-1).join('.');

  const root_dir = `/tmp/${split_name}_edit/`;//directory of the root for handling given split
  const split_file = root_dir + new_record.s3.object.key;
  const raw_images = root_dir + "raw-images";


  try{
    //Updata DynamoDB DB status. let webserver know the current status
    await DB.update({
      TableName: edit_table,
      Key:{
        "id": partation_name
      },
      UpdateExpression: `set #s = :s`,
      ExpressionAttributeNames :{
        "#s": "status"
      },
      ExpressionAttributeValues:{
        ":s": "Editing"
      }
    }).promise();


    //Create tmp directory
    await fs.mkdir(root_dir);
    await fs.mkdir(raw_images);

    //Download split from src S3
    await pipeline(
        s3.getObject({
            Bucket: new_record.s3.bucket.name,
            Key: new_record.s3.object.key
        })
        .createReadStream(),
        fs.createWriteStream(split_file)
    );

    //Untar
    await pipeline(
        fs.createReadStream(split_file),
        tar.x({
            strip: 1,
            C: raw_images
        })
    );
    fs.removeSync(split_file);


    //apply edit
    ///extract editing
    const option = await DB.get({
        TableName: edit_table,
        Key:{
          "id": partation_name
        }
    }).promise();
    ////Create a list of promise for applying edit
    let image_promise = await edit_apply(option, raw_images);
    ////make sure all the edit promise is resolved
    await Promise.all(image_promise);


    //pack all edited images into tgz
    await pipeline(
      tar.c({
        gzip: true,
        C: `${raw_images}`
      },["."]),
      fs.createWriteStream(split_file)
    );


    //Upload to dst S3
    let tar_read = fs.createReadStream(split_file);
    await s3.putObject({
        Bucket: dstS3,
        Key: new_record.s3.object.key,
        Body: tar_read
    })
    .promise();


    //Delete original object from src S3
    await s3.deleteObject({
      Bucket: new_record.s3.bucket.name,
      Key: new_record.s3.object.key
    })
    .promise();

  }
  finally{
    fs.removeSync(root_dir);
  }
};


async function edit_apply(option, raw_images){
  let image_promise = [];
  option = option.Item;

  let font;
  if(!(option.edit.text === '')) {
    font = await Jimp.loadFont(Jimp.FONT_SANS_32_WHITE);
  }

  fs.readdirSync(raw_images).forEach(file => {
      ////Push the Promise to the list, the promise will resolve when all the image is edited
      image_promise.push(
          new Promise((resolve, reject)=>{
              Jimp.read(`${raw_images}/${file}`).then((image)=> {
                if(option.resizeFactor) image.scale(Number(option.resizeFactor));
                if(option.blur=== 'true') image.blur(2);
                if(option.blackWhite=== 'true') image.greyscale();
                if(!(option.edit.text === '')) image.print(font, Number(option.edit.x), Number(option.edit.y), option.edit.text);
                image.write(`${raw_images}/${file}`, resolve());
              })
              .catch(err => {
                  throw err;
              });
          })
      );
  });
  return image_promise;
}
