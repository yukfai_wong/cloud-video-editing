//Build in
const util = require('util');
const pipeline = util.promisify(require('stream').pipeline);
const exec = util.promisify(require('child_process').exec);

//Layers
const AWS = require('aws-sdk');
const fs = require('fs-extra');
const tar = require('tar');

//Global
const s3 = new AWS.S3();
const dstS3 = "n10670815-decode";
const DB = new AWS.DynamoDB.DocumentClient();
const edit_table = "video-decoder-settings";

const NUM_IMAGES_PER_FOLDER = 50;

//AWS Event handler
exports.handler = async function(event, context) {

  const latest_record = event.Records[0];

  const video_name = latest_record.s3.object.key.split('.')[0];
  const root_dir = `/tmp/${video_name}_decode/`;//directory of the root for handling given video
  const video_file = root_dir + latest_record.s3.object.key;//path of the video
  const video_split = root_dir + "raw-frames";//the directory of the decodeed video's image list
  const video_partition = root_dir + "partition";

  try{
    await DB.update({
      TableName: edit_table,
      Key:{
        "id": video_name
      },
      UpdateExpression: `set #s = :s`,
      ExpressionAttributeNames :{
        "#s": "status"
      },
      ExpressionAttributeValues:{
        ":s": "Decoding"
      }
    }).promise();

    //Download video from src S3
    await fs.mkdir(root_dir);
    await pipeline(
      s3.getObject({
        Bucket: latest_record.s3.bucket.name,
        Key: latest_record.s3.object.key
      })
      .createReadStream(),
      fs.createWriteStream(video_file)
    );

    //Decoding
    await fs.mkdir(video_split);
    await exec(`/opt/ffmpeg-4.3.1/ffmpeg -i ${video_file} ${video_split}/%d.png`);

    //partationing
    ////Directory create
    const frames = fs.readdirSync(video_split);
    const total_partation = Math.ceil(frames.length / NUM_IMAGES_PER_FOLDER);

    await fs.mkdir(video_partition);
    for (let i = 1; i <= total_partation; i++) {
      await fs.mkdir(`${video_partition}/${video_name}.${i}.${total_partation}`);//<name>.<partition>.<totalPartation>
    }
    ////Move image into directory
    let folderNum = 1;
    let imagesCopied = 1;
    ////// iterating through the images
    for (let i = 1; i <= frames.length; i++) {
      imagesCopied++;

      //this will test to see if the cap of the number of images is above the threshold
      if (imagesCopied > NUM_IMAGES_PER_FOLDER) {
        folderNum++;
        imagesCopied = 1;
      }

      // moving the image from the original folder to the partition folder
      fs.moveSync(
        `${video_split}/${i}.png`,
        `${video_partition}/${video_name}.${folderNum}.${total_partation}/${i}.png`
      );
    }

    //cleanup
    await fs.remove(`${video_split}`);

    //tar and push to S3
    for (let i = 1; i <= total_partation; i++) {
      await pipeline(
        tar.c({
          gzip: true,
          C: `${video_partition}/${video_name}.${i}.${total_partation}`
        },["."]),
        fs.createWriteStream(`${root_dir}/${video_name}.${i}.${total_partation}.tgz`)
      );

      //Upload to dst S3
      let temp_video_read = fs.createReadStream(`${root_dir}/${video_name}.${i}.${total_partation}.tgz`);
      const UploadPromise = s3.putObject({
        Bucket: dstS3,
        Key: `${video_name}.${i}.${total_partation}.tgz`,
        Body: temp_video_read
      })
      .promise();
      console.log(`${video_name}.${i}.${total_partation}.tgz`);
      await UploadPromise;
    }


    //Delete original object from src S3
    await s3.deleteObject({
      Bucket: latest_record.s3.bucket.name,
      Key: latest_record.s3.object.key
    })
    .promise();
  }
  catch (e) {
    throw e;
  }
  finally{
    await exec(`rm -rf ${root_dir}`);
  }
};
