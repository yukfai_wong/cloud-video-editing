//Build in
const util = require('util');
const pipeline = util.promisify(require('stream').pipeline);
const exec = util.promisify(require('child_process').exec);

//Layers
const AWS = require('aws-sdk');
const fs = require('fs-extra');
const tar = require('tar');
const archiver = require('archiver');

//Global
const s3 = new AWS.S3();
const dstS3 = "n10670815-finished";

const DB = new AWS.DynamoDB.DocumentClient();
const edit_table = "video-decoder-settings";

exports.handler = async function(event, context) {
  const new_record = event.Records[0];
  const src_bucket = new_record.s3.bucket.name;

  const name_split = new_record.s3.object.key.split('.');
  const partation_prefix = name_split.slice(0,1).join();//prefix of all the partation
  const total_partation = Number(name_split.slice(2,3)[0]);


  const partation_all_key = await getAllKeys({Bucket: src_bucket, Prefix: partation_prefix});


  if(partation_all_key.length===total_partation){//if all partation retrieve
    const tmp_root = `/tmp/${partation_prefix}_merge/`;
    const partation_dir = `${tmp_root}/partation/`;
    const merge_dir = `${tmp_root}/merge/`;

    try{
      //Updata DynamoDB DB status. let webserver know the current status
      await DB.update({
        TableName: edit_table,
        Key:{
          "id": partation_prefix
        },
        UpdateExpression: `set #s = :s`,
        ExpressionAttributeNames :{
          "#s": "status"
        },
        ExpressionAttributeValues:{
          ":s": "Encoding"
        }
      }).promise();

      //Create tempory directory
      await fs.mkdir(tmp_root);
      await fs.mkdir(partation_dir);
      await fs.mkdir(merge_dir);

      //download each partation from s3 and store in /tmp/<partation_prefix>/partation
      await Promise.all(partation_all_key.map(async (key) => {
        //download a partation and write loacl
        await pipeline(
          s3.getObject({//download a partation
              Bucket: src_bucket,
              Key: key
          })
          .createReadStream(),
          fs.createWriteStream(partation_dir+key)//write partation into local
        );
        //untgz the local
        await pipeline(
          fs.createReadStream(partation_dir+key),//read partation from local
          tar.x({//untar to /tmp/<partation_prefix>/merge/
              strip: 1,
              C: merge_dir
          })
        );
        fs.removeSync(partation_dir+key);//remove partation tar
      }));


      //zip the result
      let out = fs.createWriteStream(tmp_root+`${partation_prefix}.zip`);
      var archive = archiver('zip', { zlib: { level: 9 } });
      ////ensure the Stream is close before upload
      await new Promise((resolve, reject) => {
        out.on('close', function() {
          resolve();
        });
        archive.pipe(out);
        fs.readdirSync(merge_dir).forEach(image =>{
          archive.file(merge_dir+image, {name: image});
        });
        archive.finalize();
    });


    //upload final result to S3
    let tar_read = fs.createReadStream(tmp_root+`${partation_prefix}.zip`);
    await s3.putObject({
      Bucket: dstS3,
      Key: `${partation_prefix}.zip`,
      Body: tar_read
    })
    .promise();

    //Updata DynamoDB DB status. let webserver know the current status
    await DB.update({
      TableName: edit_table,
      Key:{
        "id": partation_prefix
      },
      UpdateExpression: `set #s = :s`,
      ExpressionAttributeNames :{
        "#s": "status"
      },
      ExpressionAttributeValues:{
        ":s": "Finished"
      }
    }).promise();

    //Delete original object from src S3
    await Promise.all(partation_all_key.map(async (key) => {
      await s3.deleteObject({
        Bucket: src_bucket,
        Key: key
      })
      .promise();
    }));
    return "succeed";
  }
  finally{
    fs.removeSync(tmp_root);
  }

  }
  return `Some partation did not finish ${partation_all_key.length}/${total_partation}`;
};

async function getAllKeys(params,  allKeys = []){
  const response = await s3.listObjectsV2(params).promise();
  response.Contents.forEach(obj => allKeys.push(obj.Key));

  if (response.NextContinuationToken) {
    params.ContinuationToken = response.NextContinuationToken;
    await getAllKeys(params, allKeys); // RECURSIVE CALL
  }
  return allKeys;
}
