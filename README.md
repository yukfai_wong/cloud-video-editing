## Scalable online video editing webapp ##
This web application is attempted to simplify the video editing process by moving the computation to the cloud. This application will gives easy access of video editing function for the user which means they don’t have to go through the trouble of downloading and installing a program for simple tasks.

For the purpose of scaling the application out to handle large user base, the server-side is separated into two main part according to its responsibility: 

1. Webserver

The webserver is scaled by the load balancer and responsible for handle web access. 

2. File handling(code under \/Lambda-function)

The file handling relies on the trigger of AWS Lambda function by the object creation event of AWS S3 bucket. By partitioning the video file into small image set and trigger multiple AWS Lambda function instances, the editing can be handled concurrently. This procedure gives an advantage that the cost of processing the video file is charged by actual using time.

For the full architecture of the application check out Architecture section in report.pdf.

### Setup ###
Webserver: 
```bash
# Install dependencies for server
npm install

# Install dependencies for client
npm run client-install

# Run the client & server with concurrently
npm run dev

# Run the Express server only
npm run server

# Run the React client only
npm run client

# Server runs on http://localhost:5000 and client on http://localhost:3000
```


