require('dotenv').config();

const express = require('express');
const router = express.Router();
const AWS = require('aws-sdk');
const authenticateToken = require('../authenticateToken');

router.get('/videos', authenticateToken, (req, res) => {
  //get the users email and find all records of the videos they have processed from dynamoDB
  const email = req.email;

  //get the list of videos from the database
  const awsConfig = {
    region: 'ap-southeast-2',
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_KEY,
  };
  AWS.config.update(awsConfig);

  //gettings the users video
  const loginParams = {
    TableName: 'video-decoder-settings',
    FilterExpression: '#user = :userVal',
    ExpressionAttributeNames: {
      '#user': 'user',
    },
    ExpressionAttributeValues: { ':userVal': email },
  };

  const docClient = new AWS.DynamoDB.DocumentClient();

  docClient.scan(loginParams, (err, data) => {
    if (err) res.status(500).send(err);
    res.send(data);
  });
});

router.get('/download', (req, res) => {
  const s3 = new AWS.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_KEY,
  });
  const params = {
    Bucket: 'n10670815-finished',
    Key: req.query.id + '.zip',
    Expires: 60 * 5,
  };

  //getting the url to download the video
  s3.getSignedUrl('getObject', params, (err, url) => {
    if (err) res.status(500).send(err);
    else res.status(200).send(url);
  });
});

module.exports = router;
