require('dotenv').config();

const express = require('express');
const AWS = require('aws-sdk');
const bcrypt = require('bcryptjs');
const router = express.Router();
const jwt = require('jsonwebtoken');
const authenticateToken = require('../authenticateToken');

const awsConfig = {
  region: 'ap-southeast-2',
  accessKeyId: process.env.AWS_ACCESS_KEY,
  secretAccessKey: process.env.AWS_SECRET_KEY,
};

router.get('/signup', (req, res) => {
  AWS.config.update(awsConfig);
  const loginParams = {
    TableName: 'Video-Decoder-Users',
    Key: {
      email_id: req.query.email,
    },
  };

  const docClient = new AWS.DynamoDB.DocumentClient();

  //checking if the user is already exists
  docClient.get(loginParams, (err, data) => {
    if (Object.keys(data).length !== 0) {
      //Means the user is already made
      res.sendStatus(401);
      return;
    }

    // hashing the password so it is not stored a plain text in the database
    bcrypt.hash(req.query.password, 10, (err, hash) => {
      const params = {
        TableName: 'Video-Decoder-Users',
        Item: {
          email_id: req.query.email,
          password: hash,
          firstName: req.query.firstName,
          lastName: req.query.lastName,
        },
      };

      //adding the user to the database
      docClient.put(params, function (err, data) {
        if (err) {
          res.status(403).send(err);
        } else {
          res.status(200).send('Added the user to the database');
        }
      });
    });
  });
});

router.get('/login', (req, res) => {
  AWS.config.update(awsConfig);
  const { email } = req.query;

  const docClient = new AWS.DynamoDB.DocumentClient();

  var params = {
    TableName: 'Video-Decoder-Users',
    Key: {
      email_id: req.query.email,
    },
  };

  docClient.get(params, function (err, data) {
    if (err) {
      res.status(403).send(err);
    } else {
      if (data.Item) {
        // Compare the password given to the hashed password
        bcrypt.compare(req.query.password, data.Item.password, function (
          err,
          result
        ) {
          if (result === true) {
            const user = {
              email: email,
            };
            //making a JWT token for user session if the passwords match
            const accessToken = jwt.sign(user, process.env.ACCESS_TOKEN_SECRET);
            res.json({
              accessToken: accessToken,
            });
          } else {
            res.status(403).send({
              error: true,
              message: 'The password does not match the one on file.',
            });
          }
        });
      } else {
        res.status(403).send({
          error: true,
          message: 'The password does not match the one on file.',
        });
      }
    }
  });
});

router.get('/currentUser', authenticateToken, (req, res) => {
  //get the users email and find all records of the videos they have processed from dynamoDB
  const email = req.email;

  res.send(email);
});

module.exports = router;
