const express = require('express');
const fs = require('fs-extra');
const uuid = require('uuid');
const fileUpload = require('express-fileupload');
const AWS = require('aws-sdk');
const users = require('./Database/users');
const uploads = require('./Database/uploads');
require('dotenv').config();
const path = require('path');

const PORT = 80;

const app = express();

//serving the react app
app.use(express.static('./client/build'));

app.use(fileUpload());

app.use('/users', users);
app.use('/uploads', uploads);

app.post('/upload', (req, res) => {
  const id = uuid.v4();
  if (req.files === null) {
    return res.status(400).json({ msg: 'No files found' });
  }

  const file = req.files.file;

  const originalFileName = file.name;

  //rename the file for prevent users from uploading file with same name
  file.name = `${id}.mp4`;

  //moving the file uploaded to the root directory
  file.mv(`${__dirname}/${file.name}`, (err) => {
    if (err) {
      return res.status(500).send(err);
    }
  });

  const awsConfig = {
    region: 'ap-southeast-2',
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_KEY,
  };

  AWS.config.update(awsConfig);

  // add video settings to dynamoDB
  const docClient = new AWS.DynamoDB.DocumentClient();

  const params = {
    TableName: 'video-decoder-settings',
    Item: {
      id: id,
      originalFileName: originalFileName,
      edit: {
        text: req.query.text,
        x: req.query.x,
        y: req.query.y,
      },
      resizeFactor: req.query.resizeFactor,
      blur: req.query.blur,
      blackWhite: req.query.blackWhite,
      user: req.query.user,
      status: 'Uploading',
      uploadedOn: Date.now(),
    },
  };

  // adding the data to the table
  docClient.put(params, function (err, data) {
    if (err) {
      res.status(500).send({
        message: 'An error occured',
      });
    } else {
      console.log('added the settings to the table');

      const bucketName = 'n10670815-video-upload';
      const key = file.name;
      const uploadFile = fs.readFileSync(`${__dirname}/${file.name}`);

      //adding the file to S3
      const objectParams = { Bucket: bucketName, Key: key, Body: uploadFile };

      console.log('Uploading File to S3 Bucket');
      //make the key into the ENVIRONMENT VARIABLES INSTEAD
      const uploadPrimise = new AWS.S3({
        accessKeyId: process.env.AWS_ACCESS_KEY,
        secretAccessKey: process.env.AWS_SECRET_KEY,
      })
        .putObject(objectParams)
        .promise();

      uploadPrimise.then((data) => {
        console.log('Uploaded to AWS S3');
        res.status(200).send('Succesfully upload to the S3 Bucket');
        console.log(data);

        //deleting the file from the instance once its uploaded to S3
        fs.remove(`${__dirname}/${file.name}`)
          .then(() => {
            console.log('Completed');
          })
          .catch((err) => console.log(err));
      });
    }
  });
});

app.use((req, res) => {
  res.sendFile(path.join(__dirname, './client/build', 'index.html'));
});

app.listen(PORT, () => `Server running on port ${PORT}`);
