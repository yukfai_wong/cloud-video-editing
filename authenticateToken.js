require('dotenv').config();

const jwt = require('jsonwebtoken');

//Midleware function that we deserialize the JWT token and give the user information
const authenticateToken = (req, res, next) => {
  // getting the bearer token from the user
  const authHeader = req.headers['authorization'];

  //getting the actual token id
  const token = authHeader && authHeader.split(' ')[1];

  //making sure the user supplied a token
  if (token === null) return res.sendStatus(401);

  // checking the user details from the token
  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
    if (err) return res.sendStatus(403);

    req.email = user.email;
    next();
  });
};

module.exports = authenticateToken;
